define([], function() {

    "use strict";

    function NOOP() {
    }

    //Bookkeeping-constants: offsets of where the handlers are stored in the listener array
    var ITEMS_PER_LISTENER = 4;
    var CONTINUATION_PROMISE_OFFSET = 3;
    var PROGRESS_HANDLER_OFFSET = 2;
    var REJECT_HANDLER_OFFSET = 1;
    var RESOLVE_HANDLER_OFFSET = 0;

    function isThenable(promiseOrValue) {
      return promiseOrValue && typeof promiseOrValue === 'object' && typeof promiseOrValue.then === 'function';
    }


    function when(promiseOrValue, successHandler, errorHandler, progressHandler) {


      var p;
      if (successHandler === undefined) {//overloaded (when no callbacks are specified).
        if (isThenable(promiseOrValue)) {
          return promiseOrValue;
        }
        p = new Promise();
        p.resolve(promiseOrValue);
        return p;
      }

      //dojo-style (when callbacks are specified)
      var continuationPromise;
      if (isThenable(promiseOrValue)) {
        return promiseOrValue.then(successHandler, errorHandler, progressHandler);
      }

      p = new Promise();
      continuationPromise = p.then(successHandler, errorHandler);
      p.resolve(promiseOrValue);
      return continuationPromise;
    }

    //function to transparently reject a promise or value.
    function rejectWithError(promiseOrValue, errorHandler) {

      if (isThenable(promiseOrValue.then)) {
        return promiseOrValue.then(NOOP, errorHandler);
      }

      var p = new Promise();
      var res = p.then(NOOP, errorHandler);
      setTimeout(function() {
        p.reject(promiseOrValue);
      }, 0);
      return res;

    }

    function propagateResult(continuationPromise, listener, value) {
      var ret;
      try {
        ret = listener(value);
      } catch (e) {
        continuationPromise.reject(e);
        return;
      }

      if (isThenable(ret)) {
        ret.then(
            function(e) {
              continuationPromise.resolve(e);
            },
            function(e) {
              continuationPromise.reject(e);
            }
        );
      } else {
        continuationPromise.resolve(ret);
      }

    }


    function Promise() {
      this._resolved = false;
      this._rejected = false;
      this._listeners = null;//will lazily initialize.
      this._resolution = null;
      this._error = null;
      Object.seal(this);
    }

    Promise.prototype = {

      constructor: Promise,

      __breakIfComplete: function() {//Do not allow promise to be resolved or rejected more than once.
        if (this._resolved) {
          throw new Error('This promise has already been resolved.');
        } else if (this._rejected) {
          throw new Error('This promise has already been rejected.');
        }
      },

      __notify: function(offset, value) {//Propagate the resolution/rejection/progress value (indicated by offset) to all the listeners

        if (!this._listeners) {
          return;
        }

        var i, l;
        for (i = 0, l = this._listeners.length; i < l; i += ITEMS_PER_LISTENER) {
          propagateResult(this._listeners[i + CONTINUATION_PROMISE_OFFSET], this._listeners[i + offset], value);
        }
      },

      thenable: function() {
        var self = this;
        return {
          then: function(onResolve, onReject, onProgress) {
            return self.then(onResolve, onReject, onProgress);
          }
        };
      },

      then: function(onResolve, onReject, onProgress) {


        onReject = onReject || NOOP;
        onProgress = onProgress || NOOP;

        if (this._resolved) {
          return when(this._resolution, onResolve, onReject);
        } else if (this._rejected) {
          return rejectWithError(this._error, onReject);
        }

        if (!this._listeners) {
          this._listeners = [];
        }

        var index = this._listeners.length;
        var continuationPromise = new Promise();
        this._listeners[index + RESOLVE_HANDLER_OFFSET] = onResolve;
        this._listeners[index + REJECT_HANDLER_OFFSET] = onReject;
        this._listeners[index + PROGRESS_HANDLER_OFFSET] = onProgress;
        this._listeners[index + CONTINUATION_PROMISE_OFFSET] = continuationPromise;


        return continuationPromise;

      },

      progress: function(intermediateValue) {

        this.__breakIfComplete();

        if (!this._listeners) {
          return;
        }

        var i, l, listener;
        for (i = PROGRESS_HANDLER_OFFSET, l = this._listeners.length; i < l; i += ITEMS_PER_LISTENER) {
          listener = this._listeners[i];
          listener(intermediateValue);
        }
      },
      resolve: function(resolution) {
        this.__breakIfComplete();
        this._resolved = true;
        this._resolution = resolution;
        this.__notify(RESOLVE_HANDLER_OFFSET, resolution);
      },
      reject: function(rejection) {
        this.__breakIfComplete();
        this._rejected = true;
        this._error = rejection;
        this.__notify(REJECT_HANDLER_OFFSET, rejection);
      }
    };

    Promise.when = when;
    Promise.makeRejector = function(p) {
      return function(e) {
        return p.reject(e);
      }
    };

    Object.freeze(Promise.prototype);
    Object.freeze(Promise);

    return Promise;

  }
)
;